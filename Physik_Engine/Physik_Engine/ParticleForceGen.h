#pragma once
#include "stdafx.h"
#include "ParticleSystem.h"

class ParticleForceGen
{
public:
	virtual void updateForce(ParticleSystem *p) = 0;
};

