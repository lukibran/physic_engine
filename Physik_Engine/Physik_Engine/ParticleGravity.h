#pragma once
#include "stdafx.h"
#include "ParticleForceGen.h"
#include "ParticleSystem.h"

class ParticleGravity :
	public ParticleForceGen
{
public:
	ParticleGravity(float gX, float gY, float gZ);
	~ParticleGravity();

	void updateForce(ParticleSystem *psys);

private:
	float gravity[3];
};

