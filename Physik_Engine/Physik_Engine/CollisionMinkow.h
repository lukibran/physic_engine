#pragma once
#include "myTriangle.h"
#include "myTrianglePair.h"
#include "stdafx.h"

class CollisionMinkow
{
public:
	CollisionMinkow();
	~CollisionMinkow();

	bool detectMinkow(myTriangle* t1, myTriangle* t2);
	int pnpoly(int nvert, float *vertx, float *verty, float testx, float testy);
};

