#pragma once
#include "myTriangle.h"
#include "myTrianglePair.h"
#include "stdafx.h"

class CollisionOOBB
{
public:
	CollisionOOBB();
	~CollisionOOBB();

	bool detectOOBB(myTriangle* t1, myTriangle* t2);
};

