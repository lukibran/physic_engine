#include "CollisionBoundVol.h"



CollisionBoundVol::CollisionBoundVol()
{
}


CollisionBoundVol::~CollisionBoundVol()
{
}

bool CollisionBoundVol::detectBoundVol(myTriangle* t1, myTriangle* t2)
{
	myVec3 t1t2 = t1->m_middle->operator-(*t2->m_middle);
	float dist = sqrt(pow(t1t2.x,2) + pow(t1t2.y,2) + pow(t1t2.z,2));
	float sumRad = t1->m_radius + t2->m_radius;

	if(dist < sumRad)
	{
		return true;
	}
	return false;
}