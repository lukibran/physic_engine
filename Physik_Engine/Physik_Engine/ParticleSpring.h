#pragma once
#include "particleforcegen.h"
class ParticleSpring :
	public ParticleForceGen
{
public:
	ParticleSpring();
	~ParticleSpring();

	void updateForce(ParticleSystem *psys);

private:
	float l0;
	float springforce;
};

