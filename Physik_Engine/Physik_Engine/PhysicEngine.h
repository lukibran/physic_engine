#pragma once

#include "stdafx.h"
#include "Particle.h"
#include "ParticleSystem.h"
#include "ParticleEngine.h"
#include "ParticleForceGen.h"
#include "ParticleSpring.h"
#include "ParticleGravity.h"
#include "myTriangle.h"
#include "CollisionDetector.h"

#define WIDTH 1200
#define HEIGHT 800
#define PARTICLE 0
#define COLLISION 1
#define STATIC 0
#define DYNAMIC 1
#define MAXTRIANGLESIZE 80
#define TRIANGLECOUNT 60
#define SPRING 0

class PhysicEngine
{
public:
	PhysicEngine();
	~PhysicEngine();

	int start(const std::vector<std::string> &args);

private:
	void on_input_up(const clan::InputEvent &key);
	void jump(Particle *p,myTriangle *Obstacle);
	void on_window_close();
	bool quit;
};