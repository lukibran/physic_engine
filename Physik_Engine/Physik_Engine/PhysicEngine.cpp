#include "PhysicEngine.h"


PhysicEngine::PhysicEngine()
{
}


PhysicEngine::~PhysicEngine()
{
}

int PhysicEngine::start(const std::vector<std::string> &args)
{
	quit = false;

	//Initialisierung von Clanlib und Fenster
	DisplayWindowDescription desc;
	desc.set_title("Physic Engine");
	desc.set_size(Size(WIDTH, HEIGHT), true);
	desc.set_allow_resize(false);

	DisplayWindow window(desc);
	Canvas canvas(window);

	InputContext ic = window.get_ic();
	InputDevice keyboard = ic.get_keyboard();
	InputDevice mouse = ic.get_mouse();
	GraphicContext gc = window.get_gc();

	Slot slot_quit = window.sig_window_close().connect(this, &PhysicEngine::on_window_close);
	Slot slot_input_up = (window.get_ic().get_keyboard()).sig_key_up().connect(this, &PhysicEngine::on_input_up);
	
	srand(time(NULL));
	
	int last_time = System::get_time();
	
	float fps_timer = 0.0f;
	

#if COLLISION
	
	std::vector<myTriangle*> triangles;
	triangles.resize(TRIANGLECOUNT); 
	
	myVec3 *moveP1 = new myVec3(202.0f,60.f,0.0f);
	myVec3 *moveP2 = new myVec3(198.0f,50.0f,0.0f);
	myVec3 *moveP3 = new myVec3(194.0f,70.0f,0.0f);
	myTriangle *moveTr = new myTriangle(moveP1,moveP2,moveP3);
	myVec3 *newMiddle = new myVec3(200.0f,80.0f,0.0f);
	CollisionDetector collDetect;

	std::vector<myTriangle*> dynamicCollisions;
	
	for(unsigned int i = 0; i < triangles.size() ; i++)
	{
		float p1x = MAXTRIANGLESIZE + rand() % (WIDTH - 2*MAXTRIANGLESIZE);
		float p1y = MAXTRIANGLESIZE + rand() % (HEIGHT - 2*MAXTRIANGLESIZE);

		float p2x = p1x + 2 + (rand() % MAXTRIANGLESIZE);
		float p2y = p1y + 2 + (rand() % MAXTRIANGLESIZE);
		

		float p3x  = p1x + 2 + (rand() % MAXTRIANGLESIZE);
		float p3y  = p1y + 2 + (rand() % MAXTRIANGLESIZE);

		myVec3 *p1 = new myVec3(p1x,p1y,0);
		myVec3 *p2 = new myVec3(p2x,p2y,0);
		myVec3 *p3 = new myVec3(p3x,p3y,0);


		triangles[i] = new myTriangle(p1,p2,p3);
	}
#endif

#if PARTICLE

	CollisionDetector collDetect;

	// create a particle System & add a particle to it
	ParticleSystem* pSys = new ParticleSystem();

	//Set Initial for Particle System
	Particle* particle = new Particle(202.333328f,498.333344f,0.0f,15.0f,-30.0f, 0.0f, 1.0f);
	pSys->p.push_back(particle);

	for(int i=0;i<100;i++)
	{
		Particle* particles = new Particle(particle->pos[0],particle->pos[1],0.0f,particle->vel[0],particle->vel[1], 0.0f, 1.0f);
		pSys->p.push_back(particles);
	}

	std::vector<myTriangle*> triangles;
	triangles.resize(101); 
	for(unsigned int i = 0; i < triangles.size() ; i++)
	{
		myVec3 *p1 = new myVec3(200.0f,500.0f,0);
		myVec3 *p2 = new myVec3(205.0f,500.0f,0);
		myVec3 *p3 = new myVec3(202.0f,495.0f,0);

		triangles[i] = new myTriangle(p1,p2,p3);
	}

	//create a particleEngine
	ParticleEngine *particleEngine = new ParticleEngine();

	//Add GravityForce
	ParticleForceGen *gravGen = new ParticleGravity(0.0f, 9.81f, 0.0f);
	particleEngine->addForceGen(gravGen);

	//Floor Triangle
	myVec3 *floor1P1 = new myVec3(1.0f,600.0f,0.0f);
	myVec3 *floor1P2 = new myVec3(1100.0f,680.0f,0.0f);
	myVec3 *floor1P3 = new myVec3(1100.0f,720.0f,0.0f);
	myTriangle *floorTr1 = new myTriangle(floor1P1,floor1P2,floor1P3);

	//Roof
	myVec3 *roofP1 = new myVec3(500.0f,500.f,0.0f);
	myVec3 *roofP2 = new myVec3(1100.0f,300.0f,0.0f);
	myVec3 *roofP3 = new myVec3(1102.0f,350.0f,0.0f);
	myTriangle *roofTr = new myTriangle(roofP1,roofP2,roofP3);

#endif

#if SPRING

	ParticleEngine *particleEngine = new ParticleEngine();
	ParticleSystem* pSys = new ParticleSystem();
	std::vector<Particle*> neigh;


	//Add GravityForce
	ParticleForceGen *springGen = new ParticleSpring();
	ParticleForceGen *gravGen = new ParticleGravity(0.0f, 9.81f, 0.0f);
	particleEngine->addForceGen(gravGen);
	particleEngine->addForceGen(springGen);

	//Set Initial for Particle System
	Particle* p1= new Particle(200.0f ,200.0f,0.0f,0.0f,0.0f, 0.0f, 1.0f, true);
	Particle* p2= new Particle(300.0f ,200.0f,0.0f,0.0f,0.0f, 0.0f, 1.0f, true);	
	Particle* p3= new Particle(400.0f ,200.0f,0.0f,0.0f,0.0f, 0.0f, 1.0f, true);	
	Particle* p4= new Particle(500.0f ,200.0f,0.0f,0.0f,0.0f, 0.0f, 1.0f, true);	
	Particle* p5= new Particle(200.0f ,300.0f,0.0f,0.0f,0.0f, 0.0f, 1.0f, false);	
	Particle* p6= new Particle(300.0f ,300.0f,0.0f,0.0f,0.0f, 0.0f, 1.0f, false);	
	Particle* p7= new Particle(400.0f ,300.0f,0.0f,0.0f,0.0f, 0.0f, 1.0f, false);	
	Particle* p8= new Particle(500.0f ,300.0f,0.0f,0.0f,0.0f, 0.0f, 1.0f, false);	
	Particle* p9= new Particle(200.0f ,400.0f,0.0f,0.0f,0.0f, 0.0f, 1.0f, false);
	Particle* p10= new Particle(300.0f ,400.0f,0.0f,0.0f,0.0f, 0.0f, 1.0f, false);	
	Particle* p11= new Particle(400.0f ,400.0f,0.0f,0.0f,0.0f, 0.0f, 1.0f, false);	
	Particle* p12= new Particle(500.0f ,400.0f,0.0f,0.0f,0.0f, 0.0f, 1.0f, false);	
	Particle* p13= new Particle(200.0f ,500.0f,0.0f,0.0f,0.0f, 0.0f, 1.0f, false);	
	Particle* p14= new Particle(300.0f ,500.0f,0.0f,0.0f,0.0f, 0.0f, 1.0f, false);	
	Particle* p15= new Particle(400.0f ,500.0f,0.0f,0.0f,0.0f, 0.0f, 1.0f, false);	
	Particle* p16= new Particle(500.0f ,500.0f,0.0f,0.0f,0.0f, 0.0f, 1.0f, false);
	
	neigh.push_back(p2);
	neigh.push_back(p5);
	p1->pushNeigh(p1,neigh);
	pSys->p.push_back(p1);
	neigh.clear();

	neigh.push_back(p1);
	neigh.push_back(p3);
	neigh.push_back(p6);
	p2->pushNeigh(p2,neigh);
	pSys->p.push_back(p2);
	neigh.clear();

	neigh.push_back(p2);
	neigh.push_back(p4);
	neigh.push_back(p7);
	p1->pushNeigh(p3,neigh);
	pSys->p.push_back(p3);
	neigh.clear();

	neigh.push_back(p3);
	neigh.push_back(p8);
	p2->pushNeigh(p4,neigh);
	pSys->p.push_back(p4);
	neigh.clear();

	neigh.push_back(p1);
	neigh.push_back(p6);
	neigh.push_back(p9);
	p2->pushNeigh(p5,neigh);
	pSys->p.push_back(p5);
	neigh.clear();

	neigh.push_back(p2);
	neigh.push_back(p5);
	neigh.push_back(p7);
	neigh.push_back(p10);
	p2->pushNeigh(p6,neigh);
	pSys->p.push_back(p6);
	neigh.clear();

	neigh.push_back(p3);
	neigh.push_back(p6);
	neigh.push_back(p11);
	neigh.push_back(p8);
	p1->pushNeigh(p7,neigh);
	pSys->p.push_back(p7);
	neigh.clear();

	neigh.push_back(p4);
	neigh.push_back(p7);
	neigh.push_back(p12);
	p2->pushNeigh(p8,neigh);
	pSys->p.push_back(p8);
	neigh.clear();

	neigh.push_back(p5);
	neigh.push_back(p10);
	neigh.push_back(p13);
	p1->pushNeigh(p9,neigh);
	pSys->p.push_back(p9);
	neigh.clear();

	neigh.push_back(p6);
	neigh.push_back(p9);
	neigh.push_back(p11);
	neigh.push_back(p14);
	p1->pushNeigh(p10,neigh);
	pSys->p.push_back(p10);
	neigh.clear();

	neigh.push_back(p7);
	neigh.push_back(p10);
	neigh.push_back(p12);
	neigh.push_back(p15);
	p1->pushNeigh(p11,neigh);
	pSys->p.push_back(p11);
	neigh.clear();

	neigh.push_back(p8);
	neigh.push_back(p11);
	neigh.push_back(p16);
	p1->pushNeigh(p12,neigh);
	pSys->p.push_back(p12);
	neigh.clear();

	neigh.push_back(p9);
	neigh.push_back(p14);
	p1->pushNeigh(p13,neigh);
	pSys->p.push_back(p13);
	neigh.clear();

	neigh.push_back(p13);
	neigh.push_back(p10);
	neigh.push_back(p15);
	p1->pushNeigh(p14,neigh);
	pSys->p.push_back(p14);
	neigh.clear();

	neigh.push_back(p14);
	neigh.push_back(p11);
	neigh.push_back(p16);
	p1->pushNeigh(p15,neigh);
	pSys->p.push_back(p15);
	neigh.clear();

	neigh.push_back(p12);
	neigh.push_back(p15);
	p1->pushNeigh(p16,neigh);
	pSys->p.push_back(p16);
	neigh.clear();
#endif
	
	bool once = true;

	while(!quit)
	{
		int current_time = System::get_time();
		float deltaT = static_cast<float>(current_time - last_time);
		fps_timer = deltaT/50;
		last_time = current_time;

		canvas.clear(Colorf::black);

#if COLLISION
	
		//Draw Triangles
		for(unsigned int i = 0; i < triangles.size(); i++)
		{
			//Bounding Volumes
			canvas.fill_circle(Pointf(triangles[i]->m_middle->x, triangles[i]->m_middle->y),
							triangles[i]->m_radius, Colorf::orange);
			//Bounding Volumes
			canvas.fill_circle(Pointf(triangles[i]->m_middle->x, triangles[i]->m_middle->y),
							triangles[i]->m_radius-1, Colorf::black);
	
			//AABB zeichnen
			canvas.draw_box(triangles[i]->AABBBox.min.x,triangles[i]->AABBBox.min.y,
							triangles[i]->AABBBox.max.x,triangles[i]->AABBBox.max.y, Colorf::yellow);

			/*
			//OOBB zeichnen
			canvas.fill_triangle(Pointf(triangles[i]->OOBBBox.p[0].x,triangles[i]->OOBBBox.p[0].y),
								 Pointf(triangles[i]->OOBBBox.p[2].x,triangles[i]->OOBBBox.p[2].y),
								 Pointf(triangles[i]->OOBBBox.p[1].x,triangles[i]->OOBBBox.p[1].y), Colorf::purple);
			canvas.fill_triangle(Pointf(triangles[i]->OOBBBox.p[2].x,triangles[i]->OOBBBox.p[2].y),
								 Pointf(triangles[i]->OOBBBox.p[1].x,triangles[i]->OOBBBox.p[1].y),
								 Pointf(triangles[i]->OOBBBox.p[3].x,triangles[i]->OOBBBox.p[3].y), Colorf::purple);
			*/

			//All Triangles
			canvas.fill_triangle(Pointf(triangles[i]->m_v[0]->x,triangles[i]->m_v[0]->y),
								 Pointf(triangles[i]->m_v[1]->x,triangles[i]->m_v[1]->y),
								 Pointf(triangles[i]->m_v[2]->x,triangles[i]->m_v[2]->y), Colorf::white);

			//Middles zeichnen
			canvas.fill_circle(Pointf(triangles[i]->m_middle->x, triangles[i]->m_middle->y),
							2.0, Colorf::black);			
		}

#if DYNAMIC
		dynamicCollisions = collDetect.collisionTest(moveTr, triangles);
		
		//Bewegliches Triangle
		if(keyboard.get_keycode(keycode_right))
		{
			newMiddle->x++;
		}
		if(keyboard.get_keycode(keycode_left))
		{
			newMiddle->x--;
		}
		if(keyboard.get_keycode(keycode_up))
		{
			newMiddle->y--;
		}
		if(keyboard.get_keycode(keycode_down))
		{
			newMiddle->y++;
		}


		moveTr->updateTriangle(moveTr, *newMiddle);
		canvas.fill_triangle(Pointf(moveTr->m_v[0]->x,moveTr->m_v[0]->y),
							 Pointf(moveTr->m_v[1]->x,moveTr->m_v[1]->y),
							 Pointf(moveTr->m_v[2]->x,moveTr->m_v[2]->y), Colorf::green);

		//AABB zeichnen
		canvas.draw_box(moveTr->AABBBox.min.x,moveTr->AABBBox.min.y,
						moveTr->AABBBox.max.x,moveTr->AABBBox.max.y, Colorf::yellow);
#endif
		
			
#if STATIC	
		const std::vector<myTrianglePair>& staticCollisions = collDetect.collisionTest(triangles); 


		// Unbeweglicher Collisions Check
		for(unsigned int i = 0; i < staticCollisions.size(); i++)
		{
			//Collided Triangles in Red
			canvas.fill_triangle(Pointf(staticCollisions[i].t1->m_v[0]->x,staticCollisions[i].t1->m_v[0]->y),
								 Pointf(staticCollisions[i].t1->m_v[1]->x,staticCollisions[i].t1->m_v[1]->y),
								 Pointf(staticCollisions[i].t1->m_v[2]->x,staticCollisions[i].t1->m_v[2]->y), Colorf::red);

			canvas.fill_triangle(Pointf(staticCollisions[i].t2->m_v[0]->x,staticCollisions[i].t2->m_v[0]->y),
								 Pointf(staticCollisions[i].t2->m_v[1]->x,staticCollisions[i].t2->m_v[1]->y),
								 Pointf(staticCollisions[i].t2->m_v[2]->x,staticCollisions[i].t2->m_v[2]->y), Colorf::red);
		}
#endif
		//Beweglicher Collsion Check
		for(unsigned int i = 0; i < dynamicCollisions.size(); i++)
		{
			canvas.fill_triangle(Pointf(dynamicCollisions[i]->m_v[0]->x,dynamicCollisions[i]->m_v[0]->y),
								 Pointf(dynamicCollisions[i]->m_v[1]->x,dynamicCollisions[i]->m_v[1]->y),
								 Pointf(dynamicCollisions[i]->m_v[2]->x,dynamicCollisions[i]->m_v[2]->y), Colorf::red);
		}


#endif

#if PARTICLE

		//Update Particle Engine
		if(particle->pos[1] <= 100 && once)
		{
			for(int i=0;i<pSys->p.size();i++)
			{
				float v_x = static_cast <float> (rand()) / static_cast <float> (1000);
				float v_y = static_cast <float> (rand()) / static_cast <float> (1000);
				pSys->p[i]->vel[0] = v_x;
				pSys->p[i]->vel[1] = v_y;
			}
			once = false;
		}	
		
		//Test Against Floor
		for(int i=0; i < triangles.size(); i++)
		{
			if(collDetect.pCollision(triangles[i],floorTr1))
			{
				jump(pSys->p[i],floorTr1); 
			}
			if(collDetect.pCollision(triangles[i],roofTr))
			{
				jump(pSys->p[i],roofTr); 
			}

		}

		//Draw Floor
		canvas.fill_triangle(Pointf(floorTr1->m_v[0]->x,floorTr1->m_v[0]->y),
							 Pointf(floorTr1->m_v[1]->x,floorTr1->m_v[1]->y),
							 Pointf(floorTr1->m_v[2]->x,floorTr1->m_v[2]->y), Colorf::white);

		//Draw Roof
		canvas.fill_triangle(Pointf(roofTr->m_v[0]->x,roofTr->m_v[0]->y),
							 Pointf(roofTr->m_v[1]->x,roofTr->m_v[1]->y),
							 Pointf(roofTr->m_v[2]->x,roofTr->m_v[2]->y), Colorf::white);

		//Draw Particle
		for(int i=0; i < triangles.size(); i++)
		{
				myVec3 *newMiddle = new myVec3(pSys->p[i]->pos[0],pSys->p[i]->pos[1],0.0f);			
				canvas.fill_triangle(Pointf(triangles.at(i)->m_v[0]->x,triangles.at(i)->m_v[0]->y),
									 Pointf(triangles.at(i)->m_v[1]->x,triangles.at(i)->m_v[1]->y),
									 Pointf(triangles.at(i)->m_v[2]->x,triangles.at(i)->m_v[2]->y), Colorf::red);	
				triangles[i]->updateTriangle(triangles[i], *newMiddle);
		}

		particleEngine->updatePart(fps_timer, pSys);
#endif

#if SPRING
	
	//Linien der Federn zeichen
	for(int i=0; i < pSys->p.size(); i++)
	{
		for(int j=0; j < pSys->p[i]->neigh.size(); j++)
		{
			canvas.draw_line(pSys->p[i]->pos[0],pSys->p[i]->pos[1],pSys->p[i]->neigh[j]->pos[0],pSys->p[i]->neigh[j]->pos[1],Colorf::white);
		}
		canvas.fill_circle(Pointf(pSys->p[i]->pos[0], pSys->p[i]->pos[1]),10.0, Colorf::white);
	}

	//Auf Partikel klicken
	if(mouse.get_keycode(mouse_left))
	{
		for(int i=0; i<pSys->p.size(); i++)
		{
			Point pos = mouse.get_position();
			float dist[2]; 
			dist[0] = pos.x - pSys->p[i]->pos[0];
			dist[1] = pos.x - pSys->p[i]->pos[1];
			float length = sqrt(pow(dist[0],2) + pow(dist[1],2));
			if(length < 10 && !pSys->p[i]->fix)
			{
				pSys->p[i]->pos[0] = pos.x;
				pSys->p[i]->pos[1] = pos.y;
			}
		}
	}


	particleEngine->updatePart(0.05, pSys);
#endif
		
		canvas.flush();
		
		window.flip();
 		KeepAlive::process(0);
		
	}

	return 0;
}

void PhysicEngine::on_input_up(const InputEvent &key)
{
	if(key.id == keycode_escape)
	{
		quit = true;
	}
}

void PhysicEngine::on_window_close()
{
	quit = true;
}

void PhysicEngine::jump(Particle *p,myTriangle *Obstacle)
{
	float speedloss = 0.8f;

	myVec3 normal;
	normal.x = -(Obstacle->m_v[1]->y - Obstacle->m_v[0]->y);
	normal.y = (Obstacle->m_v[1]->x - Obstacle->m_v[0]->x);
	normal.z = 0.0f;

	normal = normal.normalize(normal);
	//float normalLenght = pow(normal.vecLenght(normal),2);

	myVec3 newVel;
	
	newVel.x = (p->vel[1]*normal.z - p->vel[2]*normal.y)*2*normal.x;
	newVel.y = (p->vel[2]*normal.x - p->vel[0]*normal.z)*2*normal.y;
	newVel.z = (p->vel[0]*normal.y - p->vel[1]*normal.x)*2*normal.z;
	
	/*
	float dot = 2*(p->vel[0]* normal.x + p->vel[1]* normal.y + p->vel[2]*normal.z);
	
	float tmp = dot/normalLenght;
	normal.x = normal.x * tmp;
	normal.y = normal.y * tmp;
	normal.z = normal.z * tmp;
	*/
	newVel.x = p->vel[0] - normal.x;
	newVel.y = p->vel[1] - normal.y;
	newVel.z = p->vel[2] - normal.z;
	
	p->vel[0] = newVel.x*speedloss;
	p->vel[1] = newVel.y*speedloss;
	p->vel[2] = newVel.z;
}
