#include "ParticleEngine.h"


ParticleEngine::ParticleEngine()
{
}


ParticleEngine::~ParticleEngine()
{
}

void ParticleEngine::addForceGen(ParticleForceGen* pfgen)
{
	 pfGen.push_back(pfgen);
}

void ParticleEngine::delForceGen(ParticleForceGen* pfgen)
{
	 for (unsigned int i = 0; i < pfGen.size(); ++i){
		if (pfGen.at(i) == pfgen){
			pfGen.erase(pfGen.begin() + i);
		}
	 }
}

void ParticleEngine::integrator(float deltaTime, ParticleSystem* psys)
{
	
	for(int i=0; i < psys->p.size(); i++)
	{
		float acc[3]; 
		float vel[3];

		for(int j=0; j < 3; j++)
		{
			// Geschwindigkeitsänderung ausrechnen 
			acc[j] = psys->p[i]->force[j] * psys->p[i]->mass/10;

			vel[j] = psys->p[i]->vel[j];

			// neue Geschwindigkeit(vel) ausrechenen
			vel[j] = vel[j] + acc[j] * deltaTime; 

			// neue Position ausrechnen
			psys->p[i]->pos[j] = psys->p[i]->pos[j] + vel[j] * deltaTime + acc[j] * 0.5 * deltaTime * deltaTime;

			//Force wieder auf 0 setzen 
			psys->p[i]->force[j] = 0;

			psys->p[i]->vel[j] = vel[j];
		}
	}	
}


void ParticleEngine::updatePart(float deltaTime, ParticleSystem* psys)
{
	// Forces zusammenrechnen
	for(int i=0; i< pfGen.size(); i++)
	{
		pfGen.at(i)->updateForce(psys);
	}

	// Integrator aufrufen
	integrator(deltaTime, psys);
}
