#pragma once
#include "myTriangle.h"
#include "myTrianglePair.h"
#include "stdafx.h"

class CollisionAABB
{
public:
	CollisionAABB();
	~CollisionAABB();

	bool detectAABB(myTriangle* t1, myTriangle* t2);
};

