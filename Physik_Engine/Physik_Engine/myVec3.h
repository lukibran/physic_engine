#pragma once 

#include <math.h>

class myVec3
{
public:

	myVec3()
	{
		x = 0.0f;
		y = 0.0f;
		z = 0.0f;
	}

	myVec3(float x, float y, float z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	myVec3 myVec3::operator-(myVec3 const &tr)
	{
		 myVec3 tmpTr = *this;

		 tmpTr.x -= tr.x;
		 tmpTr.y -= tr.y;
		 tmpTr.z -= tr.z;

		 return tmpTr;
	}

	bool operator <(const myVec3 &p)
	{
		return x < p.x || (x == p.x && y < p.y);
	}

	myVec3 myVec3::operator+(myVec3 const &tr)
	{
		 myVec3 tmpTr = *this;

		 tmpTr.x += tr.x;
		 tmpTr.y += tr.y;
		 tmpTr.z += tr.z;

		 return tmpTr;
	}

	myVec3 crossProdukt(const myVec3 &tr1, const myVec3 &tr2)
	{
		myVec3 tmp;
		tmp.x = (tr1.y*tr2.z)-(tr2.y*tr1.z);
		tmp.y = (tr1.x*tr2.z)-(tr2.x*tr1.z);
		tmp.z = (tr1.x*tr2.y)-(tr2.x*tr1.y);
		return tmp;
	}


	float dotProdukt(const myVec3 &tr1, const myVec3 &tr2)
	{
		return (tr1.x * tr2.x + tr1.y * tr2.y + tr1.z * tr2.z);
	}

	myVec3 normalize(const myVec3 &tr)
	{
		myVec3 tmp;
		float lenght = vecLenght(tr);
		tmp.x = tr.x / lenght;
		tmp.y = tr.y / lenght;
		tmp.z = tr.z / lenght;
		return tmp;

	}

	float vecLenght(const myVec3& tr)
	{
		float tmp;
		tmp = sqrt(pow(tr.x,2) + pow(tr.y,2) + pow(tr.z,2));
		return tmp;
	}




	float x;
	float y;
	float z;
};
