#pragma once
#include "myTriangle.h"

class myTrianglePair
{
public:

	myTrianglePair(myTriangle* t1, myTriangle* t2)
	{
		this->t1 = t1;
		this->t2 = t2;
	}

	myTriangle* t1;
	myTriangle* t2;
};
