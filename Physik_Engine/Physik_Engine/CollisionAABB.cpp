#include "CollisionAABB.h"


CollisionAABB::CollisionAABB(void)
{
}


CollisionAABB::~CollisionAABB(void)
{
}

bool CollisionAABB::detectAABB(myTriangle* t1, myTriangle* t2)
{
	if(t1->AABBBox.max.x < t2->AABBBox.min.x || t1->AABBBox.min.x > t2->AABBBox.max.x)
	{
		return false;
	}
	if(t1->AABBBox.max.y < t2->AABBBox.min.y || t1->AABBBox.min.y > t2->AABBBox.max.y)
	{
		return false;
	}
	if(t1->AABBBox.max.z < t2->AABBBox.min.z || t1->AABBBox.min.z > t2->AABBBox.max.z)
	{
		return false;
	}

	return true;
}
