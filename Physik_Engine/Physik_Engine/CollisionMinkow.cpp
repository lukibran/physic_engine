#include "CollisionMinkow.h"
#include "myHull.h"

CollisionMinkow::CollisionMinkow()
{
}


CollisionMinkow::~CollisionMinkow()
{
}

bool CollisionMinkow::detectMinkow(myTriangle* t1, myTriangle* t2)
{
	std::vector<myPoint> minkoPoints;

	for (int i = 0; i < 3; ++i){
		for (int j = 0; j < 3; ++j){
			myVec3 minkoDiff = *t1->m_v[i] - *t2->m_v[j];
			myPoint p;
			p.x = minkoDiff.x;
			p.y = minkoDiff.y;
			minkoPoints.push_back(p);
		}
	}

	std::vector<myPoint> points = myconvex_hull(minkoPoints);

	float* vertx = new float[points.size()];
	float* verty = new float[points.size()];
	for (int i = 0; i< points.size(); i++)
	{
		vertx[i] = points.at(i).x;
		verty[i] = points.at(i).y;
	}
	if (pnpoly(points.size(), vertx, verty, 0.0f, 0.0f) > 0){
		return true;
	}
	return false;
}

int CollisionMinkow::pnpoly(int nvert, float *vertx, float *verty, float testx, float testy)
{
	int i, j, c = 0;
	for (i = 0, j = nvert - 1; i < nvert; j = i++) {
		if (((verty[i]>testy) != (verty[j]>testy)) &&
			(testx < (vertx[j] - vertx[i]) * (testy - verty[i]) / (verty[j] - verty[i]) + vertx[i]))
			c = !c;
	}
	return c;
}
