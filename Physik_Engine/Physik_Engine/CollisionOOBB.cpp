#include "CollisionOOBB.h"


CollisionOOBB::CollisionOOBB()
{
}


CollisionOOBB::~CollisionOOBB()
{
}

bool CollisionOOBB::detectOOBB(myTriangle* t1, myTriangle* t2)
{
	return t1->OOBBBox.overlaps1Way(t2->OOBBBox);
}

