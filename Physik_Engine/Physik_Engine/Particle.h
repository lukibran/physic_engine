#pragma once
#include "stdafx.h"

class Particle
{
public:
	Particle(float posX, float posY, float posZ, float velX, float velY, float velZ, float mass);
	Particle(float posX, float posY, float posZ, float velX, float velY, float velZ, float mass, bool fixed);
	~Particle();

	void pushNeigh(Particle* p, std::vector<Particle*> neigh);

	float pos[3];
	float vel[3];
	float force[3];
	float mass;
	bool fix;

	std::vector<Particle*> neigh;
};

