#include "CollisionDetector.h"


CollisionDetector::CollisionDetector()
{
	m_trPair.reserve(10);
}


CollisionDetector::~CollisionDetector()
{
}

const std::vector<myTrianglePair>& CollisionDetector::collisionTest(std::vector<myTriangle*> triangles)
{
	CollisionBoundVol m_collBound;
	CollisionAABB m_collAABB;
	CollisionOOBB m_collOOBB;
	CollisionMinkow m_collMinkow;

	for(unsigned int i=0;i<triangles.size();i++)
	{
		for(unsigned int j=0;j<triangles.size();j++)
		{
			if(i != j)
			{
				if(m_collBound.detectBoundVol(triangles[i], triangles[j]))
				{
					m_trPair.push_back(myTrianglePair(triangles[i],triangles[j]));
				}
			}	
		}
	}

	for(unsigned int i=0;i<m_trPair.size();i++)
	{
		if(m_collAABB.detectAABB(m_trPair[i].t1,m_trPair[i].t2))
		{
			if(m_collOOBB.detectOOBB(m_trPair[i].t1,m_trPair[i].t2))
			{
				if(m_collMinkow.detectMinkow (m_trPair[i].t1,m_trPair[i].t2))
				{
					FullCollision = true;
				}
				else
				{
					FullCollision = false;
					m_trPair.erase(m_trPair.begin() + i);
				}
			}
			else
			{
				FullCollision = false;
				m_trPair.erase(m_trPair.begin() + i);
			}

		}
		else
		{
			FullCollision = false;
			m_trPair.erase(m_trPair.begin() + i);
		}
	}

	return m_trPair;

}

const std::vector<myTriangle*> CollisionDetector::collisionTest(myTriangle *movingTriangle, std::vector<myTriangle*> triangles)
{
	CollisionBoundVol m_collBound;
	CollisionAABB m_collAABB;
	CollisionOOBB m_collOOBB;
	CollisionMinkow m_collMinkow;

	for(unsigned int i=0;i<triangles.size();i++)
	{
		if(m_collBound.detectBoundVol(movingTriangle, triangles[i]))
		{
			m_hitTr.push_back(triangles[i]);
		}
	}

	for(unsigned int i=0;i<m_hitTr.size();i++)
	{
		if(m_collAABB.detectAABB(movingTriangle, m_hitTr[i]))
		{
			if(m_collOOBB.detectOOBB(movingTriangle, m_hitTr[i]))
			{
				if(m_collMinkow.detectMinkow (movingTriangle, m_hitTr[i]))
				{
					FullCollision = true;
				}
				else
				{
					FullCollision = false;
					m_hitTr.erase(m_hitTr.begin() + i);
				}
			}
			else
			{
				FullCollision = false;
				m_hitTr.erase(m_hitTr.begin() + i);
			}
		}
		else
		{
			FullCollision = false;
			m_hitTr.erase(m_hitTr.begin() + i);
		}
	}

	return m_hitTr;
}


bool CollisionDetector::pCollision(myTriangle *movingTriangle, myTriangle *obstacles)
{
	CollisionBoundVol m_collBound;
	CollisionAABB m_collAABB;
	CollisionOOBB m_collOOBB;
	CollisionMinkow m_collMinkow;

	if(m_collBound.detectBoundVol(movingTriangle, obstacles))
	{
		if(m_collAABB.detectAABB(movingTriangle, obstacles))
		{
			if(m_collOOBB.detectOOBB(movingTriangle, obstacles))
			{
				if(m_collMinkow.detectMinkow (movingTriangle, obstacles))
				{
					return true;
				}
			}
		}
	}

	return false;
}
