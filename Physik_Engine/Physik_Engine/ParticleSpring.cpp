#include "ParticleSpring.h"


ParticleSpring::ParticleSpring()
{
	l0 = 100.0f;
	springforce = 4.0f;
}


ParticleSpring::~ParticleSpring()
{
}

void ParticleSpring::updateForce(ParticleSystem *psys)
{
	for(int i=0; i < psys->p.size(); i++)
	{
		for(int j=0; j < psys->p[i]->neigh.size(); j++)
		{
			//If not Fix 
			if(!psys->p[i]->fix)
			{
				float d[3];
				float dnormal[3];

				for(int k=0; k<3; k++)
				{
					d[k] = psys->p[i]->pos[k] - psys->p[i]->neigh[j]->pos[k];	
				}	

				float dlength = sqrt(pow(d[0],2) + pow(d[1],2) + pow(d[2],2));
			
			
				for(int k=0; k<3; k++)
				{
					dnormal[k] = d[k] / dlength;
					psys->p[i]->force[k] = psys->p[i]->force[k] + (-springforce * (dlength - l0) * dnormal[k]);
				}
			}
		}		
	}
}
