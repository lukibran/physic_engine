#pragma once
#include "CollisionDetector.h"
#include "myTriangle.h"

class CollisionBoundVol
{
public:
	CollisionBoundVol();
	~CollisionBoundVol();

	bool detectBoundVol(myTriangle* t1, myTriangle* t2);
};

