#include "Particle.h"


Particle::Particle(float posX, float posY, float posZ, float velX, float velY, float velZ, float mass)
{
	
	pos[0] = posX;
	pos[1] = posY;
	pos[2] = posZ;
	vel[0] = velX;
	vel[1] = velY;
	vel[2] = velZ;

	for (int i=0; i<3; i++)
	{
		force[3] = 0.0f;
	}

	this->mass = mass;

	fix = false;

}

Particle::Particle(float posX, float posY, float posZ, float velX, float velY, float velZ, float mass, bool fixed)
{
	
	pos[0] = posX;
	pos[1] = posY;
	pos[2] = posZ;
	vel[0] = velX;
	vel[1] = velY;
	vel[2] = velZ;

	force[0] = 0.0f;
	force[1] = 0.0f;
	force[2] = 0.0f;
	

	this->mass = mass;

	fix = fixed;

}


Particle::~Particle()
{
}

void Particle::pushNeigh(Particle* p, std::vector<Particle*> n)
{
	p->neigh = n;
}
