#include "ParticleGravity.h"


ParticleGravity::ParticleGravity(float gX, float gY, float gZ)
{
		gravity[0] = gX;
		gravity[1] = gY;
		gravity[2] = gZ;	
}


ParticleGravity::~ParticleGravity()
{
}

void ParticleGravity::updateForce(ParticleSystem *psys)
{
	for(int i=0; i < psys->p.size(); i++)
	{
		for(int j=0; j < 3; j++)
		{
			if(!psys->p[i]->fix)
			{
				psys->p[i]->force[j] += gravity[j];
			}
		}		
	}
}