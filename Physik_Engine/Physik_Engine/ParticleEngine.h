#pragma once

#include "ParticleForceGen.h"
#include "ParticleSystem.h"
#include "stdafx.h"

class ParticleEngine
{
public:
	ParticleEngine();
	~ParticleEngine();

	void addForceGen(ParticleForceGen* pfgen);
	void delForceGen(ParticleForceGen* pfgen);
	void integrator(float deltaTime, ParticleSystem* psys);

	void updatePart(float deltaTime, ParticleSystem* psys);

private:
	std::vector<ParticleForceGen*> pfGen;
	ParticleSystem *pSys;
};

