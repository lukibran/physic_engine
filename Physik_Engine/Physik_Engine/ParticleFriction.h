#pragma once
#include "stdafx.h"
#include "ParticleForceGen.h"
#include "ParticleSystem.h"

class ParticleFriction :
	public ParticleForceGen
{
public:
	ParticleFriction();
	~ParticleFriction();

	void updateForce(ParticleSystem *psys);
};