#pragma once
#include "stdafx.h"
#include "CollisionAABB.h"
#include "CollisionBoundVol.h"
#include "CollisionOOBB.h"
#include "CollisionMinkow.h"
#include "myTriangle.h"
#include "myTrianglePair.h"


class CollisionDetector
{
public:
	CollisionDetector();
	~CollisionDetector();
	const std::vector<myTrianglePair>& collisionTest(std::vector<myTriangle*> triangles);
	const std::vector<myTriangle*> collisionTest(myTriangle *movingTriangle, std::vector<myTriangle*> triangles);
	bool pCollision(myTriangle *movingTriangle, myTriangle *obstacles);

private:

	std::vector<myTrianglePair> m_trPair;
	std::vector<myTriangle*> m_hitTr;
	bool FullCollision;
};


