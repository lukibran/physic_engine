#pragma once

#include "myVec3.h"
#include <algorithm>
#include <math.h>


struct AABB
{
	myVec3 min;
	myVec3 max;
};

struct OOBB
{
	void computeAxes() {
		U[0] = p[1].operator-(p[0]);
        U[1] = p[2].operator-(p[0]);

        // Make the length of each axis 1/edge length so we know any
        // dot product must be less than 1 to fall within the edge.   
		U[0].x = U[0].x / U[0].vecLenght(U[0]) * U[0].vecLenght(U[0]); 
		U[0].y = U[0].y / U[0].vecLenght(U[0]) * U[0].vecLenght(U[0]); 
	    Center.x = p[0].dotProdukt(p[0],U[0]);

		U[1].x = U[1].x / U[1].vecLenght(U[1]) * U[1].vecLenght(U[1]); 
		U[1].y = U[1].y / U[1].vecLenght(U[1]) * U[1].vecLenght(U[1]); 
	    Center.y = p[0].dotProdukt(p[0],U[1]);
        
    }

	bool overlaps1Way(OOBB& other) const 
	{
		double tMin;
        double tMax;

        for (int a = 0; a < 2; ++a) {

            double t = other.p[0].dotProdukt(other.p[0], U[a]);

            // Find the extent of box 2 on axis a
            tMin = t;
			tMax = t;

            for (int c = 1; c < 4; ++c) {
				t = other.p[c].dotProdukt(other.p[c],U[a]);

                if (t < tMin) {
                    tMin = t;
                } else if (t > tMax) {
                    tMax = t;
                }
            }
		}
            // We have to subtract off the origin

            // See if [tMin, tMax] intersects [0, 1]
        if ((tMin > 1 + Center.x) || (tMax < Center.x)) {
            // There was no intersection along this dimension;
            // the boxes cannot possibly overlap.
            return true;
            
        }

		if ((tMin > 1 + Center.y) || (tMax < Center.y)) {
            // There was no intersection along this dimension;
            // the boxes cannot possibly overlap.
            return true;
            
        }

        // There was no dimension along which there is no intersection.
        // Therefore the boxes overlap.
        return true;
    }



	myVec3 p[4];
	myVec3 Center; // OBB center point
	myVec3 U[2]; // Local x-, and y-axes
	myVec3 E; // Positive halfwidth extents of OBB along each axis
};


class myTriangle
{
public:

	myTriangle(myVec3* p1, myVec3* p2, myVec3* p3)
	{
		this->m_v[0] = p1;
		this->m_v[1] = p2;
		this->m_v[2] = p3;
		m_middle = new myVec3();
		m_middle->x = 0;
		m_middle->y = 0;
		m_middle->z = 0;
		getMiddle();
		getRadius();
		calcAABB();
		calcOOBB();
	}

	void updateTriangle(myTriangle *tr, myVec3 newMiddle)
	{
		float distX = tr->m_middle->x - newMiddle.x;
		float distY = tr->m_middle->y - newMiddle.y;

		tr->m_v[0]->x -= distX;
		tr->m_v[1]->x -= distX;
		tr->m_v[2]->x -= distX;

		tr->m_v[0]->y -= distY;
		tr->m_v[1]->y -= distY;
		tr->m_v[2]->y -= distY;

		getMiddle();
		getRadius();
		calcAABB();
		calcOOBB();
	}


	void getMiddle()
	{
		m_middle->x = ((m_v[0]->x + m_v[1]->x + m_v[2]->x) / 3);
		m_middle->y = ((m_v[0]->y + m_v[1]->y + m_v[2]->y) / 3);
		m_middle->z = (0.0f);
	}

	void getRadius()
	{
		myVec3 mp1 = m_middle->operator-(*m_v[0]);
		myVec3 mp2 = m_middle->operator-(*m_v[1]);
		myVec3 mp3 = m_middle->operator-(*m_v[2]);

		float distp1 = sqrt(pow(mp1.x,2) + pow(mp1.y,2) + pow(mp1.z,2));
		float distp2 = sqrt(pow(mp2.x,2) + pow(mp2.y,2) + pow(mp2.z,2));
		float distp3 = sqrt(pow(mp3.x,2) + pow(mp3.y,2) + pow(mp3.z,2));

		if(distp1>distp2 && distp1>distp3)
		{
			m_radius = distp1;
		}
		else if(distp2 > distp1 && distp2 > distp3)
		{
			m_radius = distp2;
		}
		else if(distp3 > distp1 && distp3 > distp2)
		{
			m_radius = distp3;
		}
	}

	

	void calcAABB()
	{
		myVec3 max;
		myVec3 min;
		float minX = 0;
		float minY = 0;
		float maxX = 0;
		float maxY = 0;

		// MAX X und MIN X ausrechnen
		if(m_v[0]->x > m_v[1]->x && m_v[0]->x > m_v[2]->x)
		{
			max.x = m_v[0]->x;
			if(m_v[1]->x > m_v[2]->x)
			{
				min.x = m_v[2]->x;
			}
			else
			{
				min.x = m_v[1]->x;
			}
		}
		else if(m_v[1]->x > m_v[0]->x && m_v[1]->x > m_v[2]->x)
		{
			max.x = m_v[1]->x;
			if(m_v[0]->x > m_v[2]->x)
			{
				min.x = m_v[2]->x;
			}
			else
			{
				min.x = m_v[0]->x;
			}
		}
		else
		{
			max.x = m_v[2]->x;
			if(m_v[0]->x > m_v[1]->x)
			{
				min.x = m_v[1]->x;
			}
			else
			{
				min.x = m_v[0]->x;
			}
		}

		// MAX Y und MIN Y ausrechenen
		if(m_v[0]->y > m_v[1]->y && m_v[0]->y > m_v[2]->y)
		{
			max.y = m_v[0]->y;
			if(m_v[1]->y > m_v[2]->y)
			{
				min.y = m_v[2]->y;
			}
			else
			{
				min.y = m_v[1]->y;
			}
		}
		else if(m_v[1]->y > m_v[0]->y && m_v[1]->y > m_v[2]->y)
		{
			max.y = m_v[1]->y;
			if(m_v[0]->y > m_v[2]->y)
			{
				min.y = m_v[2]->y;
			}
			else
			{
				min.y = m_v[0]->y;
			}
		}
		else
		{
			max.y = m_v[2]->y;
			if(m_v[0]->y > m_v[1]->y)
			{
				min.y = m_v[1]->y;
			}
			else
			{
				min.y = m_v[0]->y;
			}
		}

		AABBBox.min= min;
		AABBBox.max = max;
		
	}

	void calcOOBB()
	{
		myVec3* hypo;
		myVec3* hightPoint;
		myVec3* anka;
		myVec3* geka;

		myVec3 distAB = m_v[0]->operator-(*m_v[1]);
		myVec3 distBC = m_v[1]->operator-(*m_v[2]);
		myVec3 distAC = m_v[2]->operator-(*m_v[0]);
		if (distAB.vecLenght(distAB) > distBC.vecLenght(distBC) && distAB.vecLenght(distAB) > distAC.vecLenght(distAC))
		{
			hypo = &distAB;
			anka = &distAC;
			geka = &distBC;
			OOBBBox.p[0] = *m_v[0];
			OOBBBox.p[1] = *m_v[1];
			hightPoint = m_v[2];
		}
		else if (distBC.vecLenght(distBC) > distAB.vecLenght(distAB) && distBC.vecLenght(distBC) > distAC.vecLenght(distAC))
		{
			hypo = &distBC;
			anka = &distAB;
			geka = &distAC;
			OOBBBox.p[0] = *m_v[1];
			OOBBBox.p[1] = *m_v[2];
			hightPoint = m_v[0];
		}
		else
		{
			hypo = &distAC;
			anka = &distBC;
			geka = &distAB;
			OOBBBox.p[0] = *m_v[2];
			OOBBBox.p[1] = *m_v[0];
			hightPoint = m_v[1];
		}

		myVec3 az(0.0f, 0.0f, 1.0f);
		myVec3 normalizedHypo = hypo->normalize(*hypo);
		myVec3 hypoNomrale = normalizedHypo.crossProdukt(az, normalizedHypo);

		float s = (hypo->vecLenght(*hypo) + anka->vecLenght(*anka) + geka->vecLenght(*geka)) * 0.5f;
		float height = (2 / hypo->vecLenght(*hypo)) * sqrt(s * (s - hypo->vecLenght(*hypo))* (s - anka->vecLenght(*anka)) * (s - geka->vecLenght(*geka)));
  
		myVec3 hypoNormaleL;
		hypoNormaleL.x = hypoNomrale.x * height;
		hypoNormaleL.y = hypoNomrale.y * height;
		hypoNormaleL.z = hypoNomrale.z * height;
		myVec3 center(hypo->vecLenght(*hypo) * 0.5f, height * 0.5f, 0.0f);
		myVec3 e(hypo->vecLenght(*hypo) * 0.5f, height * 0.5f, 0.0f);

  
		OOBBBox.Center = center;
		OOBBBox.E = e;
		/*
		tmp->u[0] = *hypo;
		tmp->u[1] = hypoNormaleL;
		tmp->u[2] = *new Vertex(0.0f, 0.0f, 1.0f);
		*/
		OOBBBox.p[2] = OOBBBox.p[0] + hypoNormaleL;
		OOBBBox.p[3] = OOBBBox.p[1] + hypoNormaleL;
		
		OOBBBox.computeAxes();
	}

	OOBB OOBBBox;
	AABB AABBBox;
	myVec3* m_middle;
	myVec3* m_v[3];
	float m_radius;
};

